// Package arangodb provides some niceties for arangodb Go driver.
package arangodb

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/arangodb/go-driver"
	arangohttp "github.com/arangodb/go-driver/http"
)

// ArangoConfig provides connection details for an ArangoDB database.
type ArangoConfig struct {
	Endpoints []string `yaml:"endpoints"`
	User      string   `yaml:"user"`
	Password  string   `yaml:"password"`
	Database  string   `yaml:"database"`
}

// NewArangoDatabase creates connection to arango database from configuration.
func NewArangoDatabase(ctx context.Context, config ArangoConfig) (driver.Database, error) {
	dbCon, err := arangohttp.NewConnection(arangohttp.ConnectionConfig{
		Endpoints: config.Endpoints,
	})
	if err != nil {
		return nil, fmt.Errorf("arango connection failed: %w", err)
	}
	dbClient, err := driver.NewClient(driver.ClientConfig{
		Connection:     dbCon,
		Authentication: driver.BasicAuthentication(config.User, config.Password),
	})
	if err != nil {
		return nil, fmt.Errorf("arango authentication failed: %w", err)
	}
	db, err := dbClient.Database(ctx, config.Database)
	if err != nil {
		return nil, fmt.Errorf("could not select arango database: %w", err)
	}
	return db, nil
}

// NewDatabaseForTest creates a connection to ArangoDB suitable for Go tests.
//
// For continuous integration, the variable STERN_CI_ARANGO_CONTAINER names a
// container used for the connection to the database. Authentication is
// disabled. If the variable is empty, 127.0.0.1 is used.
//
// A new empty database is created with a random name prepended with PREFIX. If
// the prefix is empty, "gotest" is used.
//
// Usage within unit tests is discouraged. The arangodb/mocks subpackage is
// better suited for that. NewDatabaseForTests still makes sense in functional
// tests though.
func NewDatabaseForTest(ctx context.Context, prefix ...string) (driver.Database, error) {
	endpoint := "http://127.0.0.1:8529"
	container := os.Getenv("STERN_CI_ARANGO_CONTAINER")
	if container != "" {
		endpoint = "http://" + container + ":8529"
	}
	dbCon, err := arangohttp.NewConnection(arangohttp.ConnectionConfig{
		Endpoints: []string{endpoint},
	})
	if err != nil {
		return nil, err
	}

	dbClient, err := driver.NewClient(driver.ClientConfig{
		Connection: dbCon,
	})
	if err != nil {
		return nil, err
	}

	dbPrefix := "gotest"
	if len(prefix) > 0 {
		dbPrefix = strings.Join(prefix, "_")
	}

	dbName := dbPrefix + "_" + time.Now().Format("2006_01_02_15_04_05") + strconv.Itoa(rand.Int())
	return dbClient.CreateDatabase(ctx, dbName, nil)
}

// GetCollection returns the named collection and creates it if it doesn't exist
// yet. If opt is nil, default ones are used.
func GetCollection(
	ctx context.Context,
	db driver.DatabaseCollections,
	name string,
	opt *driver.CreateCollectionOptions,
) (driver.Collection, error) {
	exists, err := db.CollectionExists(ctx, name)
	if err != nil {
		return nil, fmt.Errorf("while verifying collection %s exists: %w", name, err)
	}
	if exists {
		return db.Collection(ctx, name)
	} else {
		if opt == nil {
			opt = &driver.CreateCollectionOptions{}
		}
		return db.CreateCollection(ctx, name, opt)
	}
}

// RunInTransaction encapsulates transaction handling.
//
// Thunk is invoked in a transaction started with the given collections. Thunk
// must use the given context for database operations. Error returned by thunk
// is passed through to the callee.
func RunInTransaction(
	ctx context.Context,
	db driver.Database,
	cols driver.TransactionCollections,
	thunk func(context.Context) error,
) error {
	transactionID, err := db.BeginTransaction(ctx, cols, nil)
	if err != nil {
		return fmt.Errorf("could not start transaction: %w", err)
	}

	transactionCtx := driver.WithTransactionID(ctx, transactionID)

	err = thunk(transactionCtx)
	if err != nil {
		abortErr := db.AbortTransaction(ctx, transactionID, nil)
		if abortErr != nil {
			return fmt.Errorf("failed aborting transaction: %v; because of error: %w", abortErr, err)
		}
		return err
	}

	err = db.CommitTransaction(ctx, transactionID, nil)
	if err != nil {
		return fmt.Errorf("failed committing transaction: %w", err)
	}

	return nil
}

// Upsert either creates or updates a document in the given collection.
func Upsert(ctx context.Context, col driver.Collection, sth interface{}, key string) error {
	exists, err := col.DocumentExists(ctx, key)
	if err != nil {
		return err
	}
	if exists {
		_, err = col.UpdateDocument(ctx, key, sth)
	} else {
		_, err = col.CreateDocument(ctx, sth)
	}
	return err
}

// OverwriteMode tells how to overwrite a document in Repsert.
type OverwriteMode = driver.OverwriteMode

// Repsert "replaces or inserts" entry into the collection.
//
// By default, existing entry is completely replaced. This can be changed with
// optional mode parameter.
func Repsert(
	ctx context.Context,
	collection driver.Collection,
	entry interface{},
	mode ...OverwriteMode,
) (err error) {
	repsertCtx := driver.WithOverwrite(ctx)
	if len(mode) > 0 {
		repsertCtx = driver.WithOverwriteMode(repsertCtx, mode[0])
	} else {
		repsertCtx = driver.WithOverwriteMode(repsertCtx, driver.OverwriteModeReplace)
	}
	_, err = collection.CreateDocument(repsertCtx, entry)
	return
}
