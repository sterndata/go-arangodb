# go-arangodb

Package `arangodb` provides some missing bits that make working
with `github.com/arangodb/go-driver` a bit more comfortable.

For tests, see the subpackage `mocks` for some generated mock code.
There's also `arangodb.NewDatabaseForTest` which establishes a real
Arango connection.
