// Code generated by mockery v2.13.1. DO NOT EDIT.

package mocks

import (
	context "context"

	driver "github.com/arangodb/go-driver"
	mock "github.com/stretchr/testify/mock"
)

// Index is an autogenerated mock type for the Index type
type Index struct {
	mock.Mock
}

// Deduplicate provides a mock function with given fields:
func (_m *Index) Deduplicate() bool {
	ret := _m.Called()

	var r0 bool
	if rf, ok := ret.Get(0).(func() bool); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

// Estimates provides a mock function with given fields:
func (_m *Index) Estimates() bool {
	ret := _m.Called()

	var r0 bool
	if rf, ok := ret.Get(0).(func() bool); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

// ExpireAfter provides a mock function with given fields:
func (_m *Index) ExpireAfter() int {
	ret := _m.Called()

	var r0 int
	if rf, ok := ret.Get(0).(func() int); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(int)
	}

	return r0
}

// Fields provides a mock function with given fields:
func (_m *Index) Fields() []string {
	ret := _m.Called()

	var r0 []string
	if rf, ok := ret.Get(0).(func() []string); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	return r0
}

// GeoJSON provides a mock function with given fields:
func (_m *Index) GeoJSON() bool {
	ret := _m.Called()

	var r0 bool
	if rf, ok := ret.Get(0).(func() bool); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

// ID provides a mock function with given fields:
func (_m *Index) ID() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// InBackground provides a mock function with given fields:
func (_m *Index) InBackground() bool {
	ret := _m.Called()

	var r0 bool
	if rf, ok := ret.Get(0).(func() bool); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

// MinLength provides a mock function with given fields:
func (_m *Index) MinLength() int {
	ret := _m.Called()

	var r0 int
	if rf, ok := ret.Get(0).(func() int); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(int)
	}

	return r0
}

// Name provides a mock function with given fields:
func (_m *Index) Name() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// Remove provides a mock function with given fields: ctx
func (_m *Index) Remove(ctx context.Context) error {
	ret := _m.Called(ctx)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context) error); ok {
		r0 = rf(ctx)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Sparse provides a mock function with given fields:
func (_m *Index) Sparse() bool {
	ret := _m.Called()

	var r0 bool
	if rf, ok := ret.Get(0).(func() bool); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

// Type provides a mock function with given fields:
func (_m *Index) Type() driver.IndexType {
	ret := _m.Called()

	var r0 driver.IndexType
	if rf, ok := ret.Get(0).(func() driver.IndexType); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(driver.IndexType)
	}

	return r0
}

// Unique provides a mock function with given fields:
func (_m *Index) Unique() bool {
	ret := _m.Called()

	var r0 bool
	if rf, ok := ret.Get(0).(func() bool); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

// UserName provides a mock function with given fields:
func (_m *Index) UserName() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

type mockConstructorTestingTNewIndex interface {
	mock.TestingT
	Cleanup(func())
}

// NewIndex creates a new instance of Index. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewIndex(t mockConstructorTestingTNewIndex) *Index {
	mock := &Index{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
