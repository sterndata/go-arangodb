// Code generated by mockery v2.13.1. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"

// AccessTarget is an autogenerated mock type for the AccessTarget type
type AccessTarget struct {
	mock.Mock
}

// Name provides a mock function with given fields:
func (_m *AccessTarget) Name() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

type mockConstructorTestingTNewAccessTarget interface {
	mock.TestingT
	Cleanup(func())
}

// NewAccessTarget creates a new instance of AccessTarget. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewAccessTarget(t mockConstructorTestingTNewAccessTarget) *AccessTarget {
	mock := &AccessTarget{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
